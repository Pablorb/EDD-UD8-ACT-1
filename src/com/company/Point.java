package com.company;
/**
 * Copyright
 */

/**
 * Point represent an point on the 2D coordinate Axis
 * <p>
 * An object of this class will represent a point on the negative or positive
 * plane in the infinite 2D coordinate AXIS
 *
 * @author Alex Coloma
 * @version 1.0
 */
public class Point {

    private double positionX;
    private double positionY;

    /**
     * Create a new Point object with x,y position
     * <p>
     * The point can belongs to both negative and positive plane
     * so x and y can be positive or negative
     *
     * @param positionX indicate the position on X axis
     * @param positionY indicate the position on Y axis
     */
    public Point(double positionX, double positionY) {

        this.positionX = positionX;
        this.positionY = positionY;

    }

    /**
     * Get X axis position of the point
     *
     * @return X axis position
     */
    public double getPositionX() {

        return positionX;

    }

    /**
     * Get Y axis position of the point
     *
     * @return X axis position
     */
    public double getPositionY() {

        return positionY;

    }

}
