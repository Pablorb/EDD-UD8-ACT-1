package com.company;
/**
 * Copyright 2018, IES Maria Enriquez.
 All rights reserved.
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the following disclaimer
 in the documentation and/or other materials provided with the distribution.
 Neither the name of IES Maria Enriquez. nor the names of its
 contributors may be used to endorse or promote products derived from
 this software without specific prior written permission.
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Rectangle represent a Rectangle on the 2D coordinate Axis
 * <p>
 * An object of this class will represent a rectangle on the
 * plane
 *
 * @author Pablo Redondo
 * @version 1.0
 */
public class Rectangle {

    private double width;
    private double height;
    private Point position;
    private double positionX;

    /**
     * Create a new Rectangle object with height,width values
     * <p>
     * The Rectangle belongs to the positive plane
     * so height and width have to be positive
     *
     * @param height indicate the height the Rectangle is going to have
     * @param width indicate the width the Rectangle is going to have
     */
    public Rectangle(double height, double width) {

        if ((height < 0.0 ) || (width < 0.0)) {
            throw new RuntimeException();
        }

        this.width = width;
        this.height = height;
        this.position = new Point(0.0,0.0);

    }

    public Rectangle(double height, double width, Point position) {

        this.width = width;
        this.height = height;
        this.position = position;

    }
    /**
     * Get the perimeter of the rectangle
     *
     * @return perimeter of the rectangle
     */
    public double getPerimeter(){

        return this.width*2 + this.height*2;

    }
    /**
     * Get the area of the rectangle
     *
     * @return area of a rectangle
     */
    public double getArea(){

        return this.width * this.height;

    }
    /**
     * Get the point that is the center of the Rectangle
     *
     * @return point that is the center of the rectangle
     */
    public Point getPosition(){

        return position;

    }

}
